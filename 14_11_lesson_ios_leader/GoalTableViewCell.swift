//
//  GoalTableViewCell.swift
//  14_11_lesson_ios_leader
//
//  Created by Kadir Kadyrov on 14.11.2020.
//

import UIKit

class GoalTableViewCell: UITableViewCell {
    
    @IBOutlet weak var goalLabel: UILabel!
    @IBOutlet weak var goalTimesLable: UILabel!
    @IBOutlet weak var doneView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        doneView.isHidden = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func update(title: String, curTimes: Int, timesToGoal: Int) {
        goalLabel.text = title
        goalTimesLable.text = "\(curTimes)/\(timesToGoal)"
        if curTimes == timesToGoal {
            doneView.isHidden = false
        }
    }
}
