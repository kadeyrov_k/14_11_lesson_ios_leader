//
//  FirstForHTViewController.swift
//  14_11_lesson_ios_leader
//
//  Created by Kadir Kadyrov on 14.11.2020.
//

import UIKit

class FirstForHTViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    private var name: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateNameLabel()
    }
    
    @IBAction func nextButtonTouch(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let secondVC = storyboard.instantiateViewController(identifier: "SecondForHTVC") as? SecondForHTViewController {
            secondVC.setUpVC(name: name)
            secondVC.delegate = self
            secondVC.modalPresentationStyle = .automatic
            present(secondVC, animated: true, completion: nil)
        }
    }
    
    func updateNameLabel() {
        if let name = name {
            nameLabel.text = "My name is \(name)"
        } else {
            nameLabel.text = "Tap on next"
        }
    }
}
 
extension FirstForHTViewController: SecondForHTViewControllerDelegate {
    
    func updateName(name: String) {
        self.name = name
        updateNameLabel()
    }
}
