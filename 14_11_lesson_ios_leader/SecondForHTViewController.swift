//
//  SecondForHTViewController.swift
//  14_11_lesson_ios_leader
//
//  Created by Kadir Kadyrov on 14.11.2020.
//

import UIKit

protocol SecondForHTViewControllerDelegate: class {
    func updateName(name: String)
}

class SecondForHTViewController: UIViewController {
    
    @IBOutlet weak var okButton: UIButton!
    
    private var name: String?
    weak var delegate: SecondForHTViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateOkButton()
    }
    
    func setUpVC(name: String?) {
        self.name = name
    }
    
    @IBAction func nameTextFieldChanged(_ sender: UITextField) {
        name = sender.text
        updateOkButton()
    }
    
    func updateOkButton() {
        if name != nil && name != "" {
            okButton.isEnabled = true
        } else {
            okButton.isEnabled = false
        }
    }
    
    @IBAction func okButtonTouch(_ sender: Any) {
        if let name = name {
            delegate?.updateName(name: name)
        }
        dismiss(animated: true, completion: nil)
    }
}
