//
//  NewGoalViewController.swift
//  14_11_lesson_ios_leader
//
//  Created by Kadir Kadyrov on 14.11.2020.
//

import UIKit

protocol NewGoalViewControllerDelegate: class {
    func addNewGoal(title: String, numOfTimes: Int)
}

class NewGoalViewController: UIViewController {

    @IBOutlet weak var addButoon: UIButton!
    weak var delegate: NewGoalViewControllerDelegate?
    private var name: String = ""
    private var times: Int = -1
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onAddButtonTouch(_ sender: Any) {
        delegate?.addNewGoal(title: name, numOfTimes: times)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nameOfGoalTextFieldChange(_ sender: UITextField) {
        name = sender.text ?? ""
    }
    
    @IBAction func TimesTextFieldChange(_ sender: UITextField) {
        times = Int(sender.text ?? "") ?? -1
    }
}
