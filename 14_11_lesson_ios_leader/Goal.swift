//
//  Goal.swift
//  14_11_lesson_ios_leader
//
//  Created by Kadir Kadyrov on 14.11.2020.
//

import Foundation

struct Goal {
    var title: String
    var currentTimes: Int
    var timesToGoal: Int
}
