//
//  MainViewController.swift
//  14_11_lesson_ios_leader
//
//  Created by Kadir Kadyrov on 14.11.2020.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var goalsTableView: UITableView!
    private var goals: [Goal] = [Goal]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        goalsTableView.delegate = self
        goalsTableView.dataSource = self
    }
    
    @IBAction func onAddNewGoalButtonTouch(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let newGoalVC = storyboard.instantiateViewController(identifier: "NewGoalVC") as? NewGoalViewController {
            newGoalVC.delegate = self
            newGoalVC.modalPresentationStyle = .automatic
            self.navigationController?.pushViewController(newGoalVC, animated: true)
        }
    }
    
    func addOneTime(index: Int) -> Bool {
        if goals[index].currentTimes < goals[index].timesToGoal {
            goals[index].currentTimes += 1
        }
        
        if (goals[index].currentTimes == goals[index].timesToGoal) {
            let goal = goals[index]
            goals.remove(at: index)
            goals.append(goal)
            return true
        }
        return false
    }
}

extension MainViewController: UITableViewDataSource {
    func tableView(
        _ tableView: UITableView,
        numberOfRowsInSection section: Int
    ) -> Int {
        goals.count
    }
    
    func tableView(
        _ tableView: UITableView,
        cellForRowAt indexPath: IndexPath
    ) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "GoalCell") as? GoalTableViewCell {
            cell.update(
                title: goals[indexPath.row].title,
                curTimes: goals[indexPath.row].currentTimes,
                timesToGoal: goals[indexPath.row].timesToGoal
            )
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(
        _ tableView: UITableView,
        didSelectRowAt indexPath: IndexPath
    ) {
        if addOneTime(index: indexPath.row) {
            tableView.reloadData()
        } else {
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteGoal = UIContextualAction(style: .normal, title: "") { (contextualAction, view, boolValue) in
            boolValue(true)
            
            self.goals.remove(at: indexPath.row)
            tableView.reloadData()
        }
        deleteGoal.image = UIImage(systemName: "trash.fill")
        deleteGoal.backgroundColor = .red
        
        return UISwipeActionsConfiguration(actions: [deleteGoal])
    }
}

extension MainViewController: UITableViewDelegate {
    
}

extension MainViewController: NewGoalViewControllerDelegate {
    func addNewGoal(title: String, numOfTimes: Int) {
        goals.append(Goal(title: title, currentTimes: 0, timesToGoal: numOfTimes))
        goalsTableView.reloadData()
    }
}
